const 
    search = document.querySelector('.search'),
    button = document.querySelector('.btn'),
    input = document.querySelector('.input')

button.addEventListener('click', () => {
    search.classList.toggle('active')
    input.focus()
})

