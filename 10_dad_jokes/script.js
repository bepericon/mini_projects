const 
    elJoke = document.getElementById('joke'),
    btnJoke = document.getElementById('btnJoke')

// Using then
//  const generateJoke = () => {
//      const config = {
//          headers: {
//              'Accept': 'application/json'
//          }
//      }

//      fetch('https://icanhazdadjoke.com', config)
//         .then(res => res.json())
//         .then(data => elJoke.innerHTML = data.joke)
//  }

// Using async/await
 const generateJoke = async () => {
    const
        config = {
            headers: {
                'Accept': 'application/json'
            }
        },
        res = await fetch('https://icanhazdadjoke.com', config),
        data = await res.json()

    elJoke.innerHTML = data.joke
 }

 generateJoke()

 btnJoke.addEventListener('click', generateJoke)