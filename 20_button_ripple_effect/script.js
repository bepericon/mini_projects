const buttons = document.querySelectorAll('.ripple');

buttons.forEach((button) => {
    button.addEventListener('click', function (e) {
        const { clientX, clientY } = e;
        const { offsetTop, offsetLeft } = e.target;

        const xInside = clientX - offsetLeft,
            yInside = clientY - offsetTop;

        const circle = document.createElement('span');
        circle.classList.add('circle');
        circle.style.top = `${yInside}px`;
        circle.style.left = `${xInside}px`;

        this.appendChild(circle);

        setTimeout(() => circle.remove(), 500);
    });
});
