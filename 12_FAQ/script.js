const toggle_buttons = document.querySelectorAll('.faq-toggle');

toggle_buttons.forEach(t_btn => {
    t_btn.addEventListener('click', () => {
        t_btn.parentNode.classList.toggle('active');
    });
});
