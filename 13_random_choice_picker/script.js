const tagsEl = document.getElementById('tags'),
    textarea = document.getElementById('textarea'),
    button = document.querySelector('.clear');

button.addEventListener('click', () => {
    textarea.value = '';
    tagsEl.innerHTML = '';
});

textarea.focus();

textarea.addEventListener('keyup', (e) => {
    createTags(e.target.value);

    if (e.target.value !== '\n' && e.key === 'Enter') {
        randomSelect();
    }
});

const createTags = (input) => {
    const tags = input
        .split(',')
        .filter((tag) => tag.trim() !== '')
        .map((tag) => tag.trim());

    tagsEl.innerHTML = '';
    tags.forEach((tag) => {
        const tagEl = document.createElement('span');
        tagEl.classList.add('tag');
        tagEl.innerText = tag;
        tagsEl.appendChild(tagEl);
    });
};

const randomSelect = () => {
    const times = 30,
        interval = setInterval(() => {
            const randomTag = pickRandomTag();

            highlightTag(randomTag);

            setTimeout(() => {
                unHighlightTag(randomTag);
            }, 100);
        }, 100);

    setTimeout(() => {
        clearInterval(interval);

        setTimeout(() => {
            const randomTag = pickRandomTag();
            highlightTag(randomTag);
        }, 100);
    }, times * 100);
};

const pickRandomTag = () => {
    const tags = document.querySelectorAll('.tag');
    return tags[Math.floor(Math.random() * tags.length)];
};

const highlightTag = (tag) => {
    tag.classList.add('highlight');
};

const unHighlightTag = (tag) => {
    tag.classList.remove('highlight');
};
