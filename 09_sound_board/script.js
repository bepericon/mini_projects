const sounds = [
    {id:'sw_01', name: 'Main theme'},
    {id:'sw_02', name: 'The imperial march'},
    {id:'sw_03', name: 'Duel of fates'},
    {id:'sw_04', name: 'The throne room'},
    {id:'sw_05', name: 'Binary sunset'},
]

const 
    class_play = 'fa-play-circle',
    class_pause = 'fa-pause-circle'

let current_id = ''

sounds.forEach(sound => {
    const
        {id, name} = sound, 
        btn = document.createElement('button')
    btn.classList.add('btn')

    btn.innerHTML = 
        `${name} <br>
        <i id="id_fas_${id}" class="fas ${class_play}"></i>`

    btn.addEventListener('click', () => {
        const 
            song = document.getElementById(id),
            icon = document.getElementById(`id_fas_${id}`)
        stopSongs(id)

        if(current_id){
            song.pause()
            current_id = ''
            changeIcon(icon, class_pause, class_play)
        }else{
            song.play()
            current_id = id
            changeIcon(icon, class_play, class_pause)
        }
    })

    document.getElementById('buttons').appendChild(btn)
})

const stopSongs = (idCurrent) => {
    sounds.forEach(sound => {
        const
            {id} = sound,  
            song = document.getElementById(id),
            icon = document.getElementById(`id_fas_${id}`)

        if(idCurrent != id ){
            song.pause()
            song.currentTime = 0
        }
    })
}

const changeIcon = (icon, classRemove, classAdd) => {
    icon.classList.remove(classRemove)
    icon.classList.add(classAdd)
}